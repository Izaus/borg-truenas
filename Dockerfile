FROM alpine:3

ARG BORG_VERSION

RUN set -x \
    && apk update \
    && apk upgrade \
    && apk add bash \
        openssh \ 
        python3 \
        py3-pip \
        pkgconfig \
        openssl-dev \
        gcc \
        musl-dev \
        acl-dev \
        libacl \
        python3-dev \
        linux-headers \
        shadow \
    && rm -f /etc/ssh/ssh_host_* \
    && pip install -U pip setuptools wheel --break-system-packages \
    && pip3 install -v "borgbackup==${BORG_VERSION}" --break-system-packages \
    && apk del pkgconfig \
        openssl-dev \
        gcc \
        musl-dev \
        acl-dev \
        python3-dev \
        linux-headers \
    && adduser --uid 500 --disabled-password --gecos "Borg Backup" borg \
    && mkdir -p /var/run/sshd /var/backups/borg /var/lib/docker-borg/ssh mkdir /home/borg/.ssh \
    && chown borg.borg /var/backups/borg /home/borg/.ssh \
    && chmod 700 /home/borg/.ssh

RUN set -x \
    && sed -i \
        -e 's/^#PasswordAuthentication yes$/PasswordAuthentication no/g' \
        -e 's/^PermitRootLogin without-password$/PermitRootLogin no/g' \
        -e 's/^X11Forwarding yes$/X11Forwarding no/g' \
        -e 's/^#LogLevel .*$/LogLevel ERROR/g' \
        -e 's/^#Port 22/Port 9022/g' \
        /etc/ssh/sshd_config

VOLUME ["/var/backups/borg", "/var/lib/docker-borg", "/home/borg/.ssh/authorized_keys"]

ADD ./entrypoint.sh /

EXPOSE 9022
CMD ["/entrypoint.sh"]
