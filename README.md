# Docker image for borgbackup server

I needed a way to run borg server on Truenas Scale in a container.
Because port 22 is used by underlying OS, port 9022 is exposed for borg ssh connections.

## Build

Tag should match currently used borg version. It wasn't the case until this point
```sh
docker build --pull --no-cache --build-arg BORG_VERSION=1.2.7 -t suazi/borg-truenas:[tag] .
```
**Push**
```sh
docker push suazi/borg-truenas:[tag]
```

## Create borg user in underlying system
```
groupadd -g 1100 borg && useradd -m borg -u 1100 -g 1100
```

## Run container
1. Load authorized keys from file
```sh
docker run -d -e BORG_UID=1100 -e BORG_GID=1100 -v [authorized_keys file]:/home/borg/.ssh/authorized_keys -v /home/borg/ssh:/var/lib/docker-borg -v home/borg/data:var/backups/borg -p 9022:9022 suazi/borg-server
```
## Initialize backup

1. Create entry in ssh config file for easier interaction:
```ini
Host borgprod
	Hostname [hostname]
	IdentityFile [priv_key]
	Port 9022
	User borg
```

2. Initialize backup
```sh
borg init --encryption=repokey borgprod:/var/backups/borg/krkpc
```

- password created in this step will be used for all interaction with the backup

## Create backup
```sh
borg create -v --stats borgprod:/var/backups/borg/krkpc::[repo] [dir1] [dir2] ... [...]
```

## Show backups
List archives in repo:
```sh
borg list borgprod:/var/backups/borg/krkpc
```

List content of specific archive:
```sh
borg list borgprod:/var/backups/borg/[repo]::[archive name]
```

## Restore archive

Archive is restored to current directory:
```sh
borg extract borgprod:/var/backups/borg/krkpc::[archive name]
```





